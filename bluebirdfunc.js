var promise = require('bluebird');
var fs = promise.promisifyAll(require('fs'));

inputFile= 'input.txt';
outputFile= 'output.txt';

function readAndWriteFile(inputFile,outputFile){
		console.log('Reading file '+inputFile);
		fs.readFileAsync(inputFile)
		.then((content)=>{
			console.log('Completed reading file '+inputFile);
			console.log('Writing file '+outputFile);
			fs.writeFileAsync(outputFile,content)
		.then((result)=>{
			console.log('Completed writing file '+outputFile);
		})
	})
	.catch((e)=>{
		console.log('error');
	})
}

readAndWriteFile(inputFile,outputFile);

// var Promise = require('bluebird');
// var fs = Promise.promisifyAll(require('fs'));
// // var fs = require('fs')

// function readAndWriteFile(inputfile, outputfile){
//   fs.readFileAsync(inputfile)
//     .then(function(filedata){
//       console.log("filedata", filedata)
//       return fs.writeFileAsync(outputfile, filedata)
//     })
//     .catch(function(e){
//       console.log("error:"+e)
//       return e
//     })
// }

// readAndWriteFile('input.txt', 'output.txt');