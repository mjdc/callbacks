var co = require('co'); 
var fs = require('fs');

inputFile= 'input.txt';
outputFile= 'output.txt';

function readFile(inputFile){
	return new Promise(function(resolve,reject){
		console.log('Reading file '+inputFile);
		fs.readFile(inputFile,function(err,result){
			if(err) {
				console.log('Error reading file '+inputFile);
				reject('Read error');
			} else {
				console.log('Completed reading file '+inputFile);
				resolve(result);
			}
		});
	});
}

function writeFile(outputFile,content){
	return new Promise ((resolve,reject)=>{
		console.log('Writing file '+outputFile);
		fs.writeFile(outputFile,content,function(err){
			if(err){
				console.log('Error writing file '+outputFile);
				reject('write error');
			} else{
				console.log('Completed Writing file '+outputFile);
				resolve('');
			}
		});
	})
}

co(function *(){
	const contents = yield readFile(inputFile);
	const read = yield writeFile(outputFile,contents);
}).catch((err)=>{
	console.log(err);
})