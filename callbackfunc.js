var fs = require('fs'); 

inputFile= 'input.txt';
outputFile= 'output.txt';

function readFile(inputFile,callback){
	console.log('Reading file '+inputFile);
	fs.readFile(inputFile,(err,content)=>{
		if(err) callback(err,null);
		else callback(null,content);
	});
}

function writeFile(outputFile,content,callback){
	console.log('Writing file '+outputFile);
	fs.writeFile(outputFile,content,(err)=>{
		if(err) callback(err,null);
		else callback(null,null);
	});
}

function readAndWrite(inputFile,outputFile,callback){
	readFile(inputFile,(err,content)=>{
		if(err) {
			console.log('Failed reading file '+inputFile);
			callback(err,null);
		}
		else{
			console.log('Completed reading file '+inputFile);
			writeFile(outputFile,content,(err,success)=>{
				if (err) {
					console.log('Failed writing file '+outputFile);
					callback(err,null);
			}
				else {
					console.log('Completed writing file '+ outputFile);
					callback(null,null);
			}
			});
		}
	});
}

readAndWrite(inputFile,outputFile,(error,success)=>{
	if(error) console.log('Failure!');
	else console.log('Ok!');
})